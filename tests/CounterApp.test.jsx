import { fireEvent, render, screen } from "@testing-library/react";
import { CounterApp } from "../src/CounterApp";

describe('Pruebas en el <CounterApp />', () => {

  const initialValue = 10;

  test('debe de hacer match con el snapshot', () => {
    const { container } = render(<CounterApp value = { initialValue }/>);
    expect(container).toMatchSnapshot(); //* creando el Snapshot, que se encarga que todo se mantenga igual
  });

  test('debe de mostrar el valor inicial de 100 <CounterApp value={100}>', () => {
    // render(<CounterApp value={10} />);
    // expect(screen.getByRole('heading', {level: 2}).innerHTML).toContain(initialValue.toString());
    render(<CounterApp value={100}/>);
    expect(screen.getByText(100)).toBeTruthy();
  });

  test('debe de incrementar con el botón +1', () => {
    //* inicializando el objeto de pruebas
    render(<CounterApp value={initialValue}/>);
    //react testing library nos ofrece el
    fireEvent.click( screen.getByText('+1')); //* estimulo
    //* estariamos esperando que de 10 cambie a 11
    expect(screen.getByText('11')).toBeTruthy();
  });

  test('debe de decrementar con el botón -1', () => {
    //* inicializando el objeto de pruebas
    render(<CounterApp value={initialValue}/>);
    //react testing library nos ofrece el
    fireEvent.click( screen.getByText('-1')); //* estimulo
    //* estariamos esperando que de 10 cambie a 11
    // screen.debug(); siempre podriamos utilizar esto para evaluar el estado DOM
    expect(screen.getByText('9')).toBeTruthy();
  });

  test('debe de funcionar el botón de reset', () => {
    //* Sujeto de pruebas
    render( <CounterApp value={ initialValue } />);
    //* Estimulos
    fireEvent.click(screen.getByText('+1'));
    fireEvent.click(screen.getByText('+1'));
    fireEvent.click(screen.getByText('+1'));
    // fireEvent.click(screen.getByText('Reset'));
    fireEvent.click(screen.getByRole('button', { name: 'btn-reset'}));

    //* despues del reset estariamos esperando, que vuelva al valor del initialValue
    expect(screen.getByText(initialValue)).toBeTruthy;
    // screen.debug();
  });



});