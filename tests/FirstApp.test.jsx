import { render } from "@testing-library/react";
import { FirstApp } from "../src/FirstApp";

describe('Pruebas en <FirstApp/>', () => {

  test('debe de hacer match con el snapshot', () => {
    // renderiza al componente en memoria
    //const title = 'Hola Mundo';
    //* el render retorna un objeto que expone ciertas propiedades
    //const { container } = render(<FirstApp title={title}/>)
    //* Con este container podemos tomar una fotografia
    // expect(container).toMatchSnapshot();
    //* si realizamos un cambio en el componente las pruebas van a fallar por que no se va corresponder con el snapshot
    /* 
    *esto nos podria servir para darnos cuenta de los errores que quizas no esperamos, ya que puede darse el caso que necesariamiente necesitamos que nuestro componente sea igual al snapshot y todo lo que fuese distinto es indicativo de error, el componente sufrio un cambio inesperado o tambien se puede dar el caso que estamos actualizando el componente y necesitamos una nueva snapshot, para eso jest nos ofrece la tecla "u", para realizar un updated del snapshot */

    //! nos ayuda a controlar que nuestro componente no cambie de forma accidental el dia de mañana
  });

  test('Debe de mostrar el título en un h1', () => {
    
    const title = 'Hola Mundo';
    //* getByText que obtenga un nodo por algun texto, nos ayuda a renderizar le DOM. el react testing library
    const { container, getByText, getByTestId } = render( <FirstApp title={title}/>);
    //* esperamos que componente renderizado este este texto
    expect(getByText(title)).toBeTruthy;
    //* el container es literalmente un nodo del dom en javascript
    // const h1 = container.querySelector('h1');
    // console.log(h1.innerHTML);

    // expect(h1.innerHTML).toBe(title); //* dice que tiene que ser exacto el string sin espacios
    // expect(h1.innerHTML).toContain(title); //* dice que el contain no le importa eso
    expect(getByTestId('test-title').innerHTML).toContain(title);
    

  });

  test('debe de mostrar el subtitulo enviado por props', () => {
    const title = 'Hola Mundo';
    const subTitle = 'Soy un subtitulo';
    const { getByText, getAllByText } = render( 
      <FirstApp 
        title= {title}
        subTitle= { subTitle }
        />
    );
    //* getByText solo se asegura que sea uno y da un error si hay mas de uno
    //* getAllByAltText devuelve un array
    // expect(getByText(subTitle)).toBeTruthy();
    expect(getAllByText(subTitle).length).toBe(2);
  });
});