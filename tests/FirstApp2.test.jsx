import { render, screen } from "@testing-library/react";
import { FirstApp } from "../src/FirstApp";

//* el screen es el objeto que estamos renderizando

describe('Pruebas en <FirstApp/>', () => {

  const title = 'Hola Mundo';
  const subTitle = 'Soy un subtitulo';
  
  test('debe de hacer match con el snapshot', () => {
    const { container } = render(<FirstApp title={title} /> );
    expect(container).toMatchSnapshot();
  });

  test('debe de mostrar el mensaje "Hola Mundo', () => {
    render(<FirstApp title={title} />);
    // screen.debug(); //* esto nos va permitir todo el componente renderizado, con las ultimas actualizaciones despues de los cambios del DOM
    expect(screen.getByText(title)).toBeTruthy(); //* que exista el texto
    //* expect(screen.getByText(title)).not.toBeTruthy(); asi es como se hace cuando quiero que no exista el texto
  });

  test('debe de mostrar el titulo en un h1', () => {
    render(<FirstApp title={title} />);
    expect(screen.getByRole('heading', { level: 1}).innerHTML).toContain(title);
  });

  test('debe de mostrar el subtitulo enviado por props', () => {

    render(<FirstApp 
      title={title}
      subTitle={subTitle}
      />);

    expect(screen.getAllByText(subTitle).length).toBe(2);
    screen.getAllByText(subTitle).forEach(sub => expect(sub.textContent).toBe(subTitle));
  });


});