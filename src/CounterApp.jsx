import { useState } from 'react';
import PropTypes from 'prop-types';

//* esta función no esta utilizando los props y tampoco nada del componente por eso se recomienda dejarla fuera del componente
//* de manera que cuando nuestro CounterApp se vuelva a renderizar no va a volver a crear este espacio en memoria
// const  handleAdd = ( event ) => { 
//   console.log(event) 
// }

export const CounterApp = ({value}) => {
  // console.log('Componente ejecutandose');
  const [ counter, setCounter] = useState(value);
  
  //* el setCounter es el que le dice a React que hay un cambio en el estado que debe volver a renderizar al componente

  //* el profe dice que las funciones de flecha no cambian el valor del this
  const  handleAdd = () => { 
    // console.log(event);
    //* no puedo utilizar el counter++ por que eso estaria intentando acumular en una constante
    setCounter(counter + 1);
    //* otra forma de hacerlo setCounter((c) => c + 1 ) en donde c toma el valor actual del counter
  }
  const handleSubtract = () => setCounter(counter - 1);
  
  const handleReset = () => setCounter(value);
  
  
  return (
    <>
      <h1>CounterApp</h1>
      <h2>{counter}</h2>
      {/* <button onClick={ (event) =>handleAdd(event)}>+1</button> */}
      {/* cuando tenemos un unico argumento y este argumento tambien es lo unico que lo estamos pasando a otra funcion
      podemos resumirlo de esta manera */}
      <button onClick={ handleAdd }>+1</button>
      <button onClick={ handleSubtract }>-1</button>
      <button aria-label="btn-reset" onClick={ handleReset }>Reset</button>
    </>
  )
}

CounterApp.propTypes = {
  value: PropTypes.number
}