import React from 'react'
import ReactDOM from 'react-dom/client'
// import { HelloWorldApp }  from './HelloWorldApp';
// import { FirstApp } from './FirstApp';
import { CounterApp } from './CounterApp';
//* css globales
import './styles.css';


ReactDOM.createRoot(document.querySelector('#root')).render(
  <React.StrictMode>
    {/* <HelloWorldApp/> */}
    {/* en el caso del subTitle necesito {} para indicarle el valor numérico */}
    {/* <FirstApp title="Hola, soy Goku" subTitle={123} /> */}
    {/* <FirstApp title="Hola Mundo" /> */}

    <CounterApp value={20}/>
  </React.StrictMode>
);