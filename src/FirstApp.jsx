import PropTypes from 'prop-types';
//* yarn add prop-types 

//* los props son objetos
export const FirstApp = ( { title, subTitle, name } ) => {
  // console.log(props);
  return (
    <>
      <h1 data-testid="test-title">{title}</h1>
      {/* <h1>{ newMessage.message }</h1> */}
      {/* <code>{ JSON.stringify(newMessage)}</code> */}
      <p>{subTitle}</p>
      <p>{subTitle}</p>
      <p>{ name }</p>
    </>
  )
}


FirstApp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string
}

// los default props entran antes que los propTypes
FirstApp.defaultProps = {
  name: 'Miguel Ramos',
  subTitle: 'No hay subtitulo',
  // title: 'No hay título',
}